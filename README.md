# UZI

A load testing tool for API's made in Rust for measuring Web API's performance. It provides a flexible facility for generating various HTTP workloads.

## Features

* Multi-threaded HTTP Request.
* Metrics Analysis and comparation.
* Query Builder.
* Fake Data insertion.
* Test Cases.
* HTTP 1.1 / 2.0 Support.
* JSON Outputs.
